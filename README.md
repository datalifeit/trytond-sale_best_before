datalife_sale_best_before
=========================

The sale_best_before module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_best_before/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_best_before)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
